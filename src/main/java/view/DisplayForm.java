package view;

import dto.Product;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Mateusz on 2016-04-19.
 */
public class DisplayForm extends JFrame {

    private static final String TITLE = "Point of sale";

    private JList<Product> products;
    private JPanel rootPanel;
    private JFormattedTextField productCode;
    private JFormattedTextField amount;
    private JButton addProductButton;
    private JFormattedTextField sum;
    private DefaultListModel<Product> listModel;

    public DisplayForm() {
        super(TITLE);
        setup();
        setContentPane(rootPanel);
        init();
    }

    private void init() {
        pack();
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocation(600, 200);

        addListModel();
        addAmountVerifier();
    }

    private void addAmountVerifier() {
        this.amount.setInputVerifier(new InputVerifier() {
            @Override
            public boolean verify(JComponent input) {
                String text = ((JFormattedTextField) input).getText();
                return text.matches("^-?\\d+$");
            }
        });
    }

    private void addListModel() {
        listModel = new DefaultListModel<>();
        this.products.setModel(listModel);
    }

    public DefaultListModel<Product> getListModel() {
        return this.listModel;
    }

    public JButton getAddProductButton() {
        return addProductButton;
    }

    public void setSum(String sum) {
        getSum().setText(sum);
    }

    public String getSumValue() {
        return getSum().getText();
    }

    public String getBarcodeCode() {
        return getProductCode().getText();
    }

    public Integer getAmountValue() {
        return Integer.valueOf(getAmount().getText());
    }

    public void showMessage(String title, String message, int messageKind) {
        JOptionPane.showMessageDialog(this, message, title, messageKind);
    }

    public void resetFields() {
        getProductCode().setText("");
        getAmount().setText("1");
    }

    private JFormattedTextField getProductCode() {
        return productCode;
    }

    private JFormattedTextField getAmount() {
        return amount;
    }

    private JList<Product> getProducts() {
        return products;
    }

    private JFormattedTextField getSum() {
        return sum;
    }

    @SuppressWarnings("unchecked")
    private void setup() {
        rootPanel = new JPanel();
        rootPanel.setLayout(new GridBagLayout());
        rootPanel.setEnabled(true);
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        GridBagConstraints gbc;
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        rootPanel.add(panel1, gbc);
        final JLabel label1 = new JLabel();
        label1.setText("Product code");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel1.add(label1, gbc);
        final JLabel label2 = new JLabel();
        label2.setText("Amount");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel1.add(label2, gbc);
        amount = new JFormattedTextField();
        amount.setText("1");
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.ipadx = 20;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel1.add(amount, gbc);
        productCode = new JFormattedTextField();
        productCode.setEditable(true);
        productCode.setHorizontalAlignment(2);
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        gbc.weightx = 1.0;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.ipadx = 150;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel1.add(productCode, gbc);
        addProductButton = new JButton();
        addProductButton.setText("Add product");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel1.add(addProductButton, gbc);
        sum = new JFormattedTextField();
        sum.setEditable(false);
        sum.setHorizontalAlignment(10);
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.weightx = 2.0;
        gbc.anchor = GridBagConstraints.SOUTHEAST;
        gbc.ipadx = 100;
        gbc.ipady = 2;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel1.add(sum, gbc);
        final JLabel label3 = new JLabel();
        label3.setHorizontalAlignment(SwingConstants.LEADING);
        label3.setText("Sum");
        gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.SOUTHEAST;
        gbc.insets = new Insets(5, 5, 5, 5);
        panel1.add(label3, gbc);
        final JScrollPane scrollPane1 = new JScrollPane();
        gbc = new GridBagConstraints();
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.gridheight = 3;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        rootPanel.add(scrollPane1, gbc);
        products = new JList();
        scrollPane1.setViewportView(products);
        label1.setLabelFor(productCode);
        label2.setLabelFor(amount);
        label3.setLabelFor(sum);
    }

}
