package mock;

import controller.auxiliary.FileManager;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


/**
 * Created by Mateusz on 2016-04-19.
 */
public class DataSource {

    private static List<String> productNames;

    static {
        productNames = Arrays.asList(
                "Coca-cola",
                "Samsung",
                "Asus",
                "Rower",
                "Słuchawki",
                "Pomidory",
                "Banany",
                "Dysk-ssd",
                "Okulary",
                "Opony"
        );
    }

    private void generateData() {
        Path productsFilePath = Paths.get("products");
        Random rand = new Random(9999999999999L);
        List<String> products = new ArrayList<>();
        for (int i = 1; i <= 500; i++) {
            Long code = rand.nextLong();
            if (code < 0) {
                code = -code;
            }
            String productName = productNames.get(i % 10);
            BigDecimal price = new BigDecimal(new BigInteger(16, rand).toString() + "." + new BigInteger(4, rand).toString());
            String product = code + " " + productName + " " + price + "\n";
            products.add(product);
        }
        FileManager.writeToFile(productsFilePath, products);
    }

}
