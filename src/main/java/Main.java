import controller.FormController;
import view.DisplayForm;

/**
 * Created by Mateusz on 2016-04-14.
 */
public class Main {

    public static void main(String[] args) {
        new FormController(new DisplayForm());
    }
}
