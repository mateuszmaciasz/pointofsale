package controller.auxiliary;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz on 2016-04-21.
 */
public class FileManager {

    public static void writeToFile(Path path, List<String> products) {
        try (FileChannel storageFile = FileChannel.open(path,
                StandardOpenOption.CREATE,
                StandardOpenOption.READ,
                StandardOpenOption.WRITE)) {

            FileLock lock = storageFile.lock(0L, Long.MAX_VALUE, false);
            BufferedWriter writer = new BufferedWriter(Channels.newWriter(storageFile, StandardCharsets.UTF_8.name()));
            for (String product : products) {
                writer.write(product);
                writer.flush();
            }

            lock.release();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<String> readFile(Path path) {
        List<String> ret = new ArrayList<>();
        try {
            FileChannel storageFile = FileChannel.open(path, StandardOpenOption.CREATE, StandardOpenOption.READ, StandardOpenOption.WRITE);
            BufferedReader reader = new BufferedReader(Channels.newReader(storageFile, StandardCharsets.UTF_8.name()));
            String read = reader.readLine();
            while (read != null) {
                ret.add(read);
                read = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
