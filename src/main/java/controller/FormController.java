package controller;

import controller.auxiliary.FileManager;
import dto.Product;
import view.DisplayForm;

import javax.swing.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Mateusz on 2016-04-19.
 */
public class FormController {

    private static final String ERROR = "Error";
    private static final String WARNING = "Warning";
    private static final String SPACE = " ";

    private DisplayForm form;
    private List<Product> products;

    public FormController(DisplayForm form) {
        this.form = form;
        this.products = new LinkedList<>();
        feedProductList();
        addButtonActionListener();
    }

    FormController() {
        this.products = new LinkedList<>();
        feedProductList();
    }

    private void addButtonActionListener() {
        getForm().getAddProductButton().addActionListener(e -> {
            String barcodeCode = getForm().getBarcodeCode();
            Integer amount = getForm().getAmountValue();
            String sumValue = getForm().getSumValue();
            control(barcodeCode, amount, sumValue);
        });
    }

    private void control(String barcodeCode, Integer amount, String sumValue) {
        BigDecimal sum = new BigDecimal(BigInteger.ZERO);
        if (barcodeCode == null || barcodeCode.isEmpty()) {
            getForm().showMessage(ERROR, "Invalid bar-code", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (sumValue != null && !sumValue.isEmpty()) {
            sum = sum.add(new BigDecimal(sumValue));
        }
        if (barcodeCode.equals("exit")) {
            DefaultListModel<Product> listModel = getForm().getListModel();
            Enumeration<Product> elements = listModel.elements();
            while (elements.hasMoreElements()) {
                System.out.println(elements.nextElement().toString());
            }
            System.out.println("Sum: " + getForm().getSumValue());
            listModel.removeAllElements();
            getForm().setSum("");
            getForm().resetFields();
            return;
        }
        Product product = findProduct(barcodeCode);
        if (product == null) {
            getForm().showMessage(WARNING, "Product not found", JOptionPane.WARNING_MESSAGE);
            return;
        }
        for (int i = 1; i <= amount; i++) {
            getForm().getListModel().addElement(product);
            sum = sum.add(product.getPrice());
        }
        getForm().setSum(sum.toString());
        getForm().resetFields();
    }

    private void feedProductList() {
        List<String> productList = FileManager.readFile(Paths.get("products"));
        productList.stream().forEach(product -> products.add(getProductFromString(product)));
    }

    Product findProduct(String code) {
        Optional<Product> searchResult = products.stream().filter(product -> product.getCode().equals(code)).findAny();
        return searchResult.isPresent() ? searchResult.get() : null;
    }

    private Product getProductFromString(String productData) {
        Product product = new Product();
        String[] splitedData = productData.split(SPACE);
        String code = splitedData[0];
        String productName = splitedData[1];
        BigDecimal price = new BigDecimal(splitedData[2]);
        product.setCode(code);
        product.setName(productName);
        product.setPrice(price);
        return product;
    }

    private DisplayForm getForm() {
        return form;
    }
}
