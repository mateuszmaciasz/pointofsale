package controller;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mateusz on 2016-04-21.
 */
public class FormControllerTest {

    @Test
    public void control() throws Exception {
        FormController controller = new FormController();
        assertNotNull(controller.findProduct("382233781285961059"));
        assertNotNull(controller.findProduct("4340335311458673228"));
        assertNull(controller.findProduct("14325423523"));
        assertNull(controller.findProduct("562423423"));
    }

}